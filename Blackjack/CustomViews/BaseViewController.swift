//
//  BaseViewController.swift
//  Blackjack
//
//  Created by Ernesto Jose Contreras Lopez on 21/7/22.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    enum NavigationStyle {
        case simple
        case hidden
    }

    // MARK: - Properties
    lazy var theme: NavigationStyle = viewControllerTheme()
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationStyle()
    }
    
    // MARK: - Setup
    private func setupNavigationStyle() {
        setupNavigationBar()
        
        var backImage = UIImage()

        switch theme {
        case .simple:
            navigationController?.setNavigationBarHidden(false, animated: false)
            backImage = UIImage(named: "backArrow") ?? UIImage()
        case .hidden:
            navigationController?.setNavigationBarHidden(true, animated: false)
        }

        navigationController?.navigationBar.backIndicatorImage = backImage
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    func viewControllerTheme() -> NavigationStyle {
        .simple
    }
    
    private func setupNavigationBar() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .clear
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance

        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
}
