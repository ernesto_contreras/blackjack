//
//  HomeViewController.swift
//  Blackjack
//
//  Created by Ernesto Jose Contreras Lopez on 21/7/22.
//

import UIKit

class HomeViewController: BaseViewController {
    // MARK: - Properties
    
    @IBOutlet private(set) weak var scrollView: UIScrollView!
    @IBOutlet private(set) weak var startButton: UIButton!
    @IBOutlet private(set) weak var dollarLabel: UILabel!
    @IBOutlet private(set) weak var amountTextField: UITextField!

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        startButton.layer.cornerRadius = startButton.frame.height / 2
        amountTextField.layer.cornerRadius = amountTextField.frame.height / 2
    }
    
    override func viewControllerTheme() -> BaseViewController.NavigationStyle {
        .hidden
    }
    
    // MARK: - Setup
    private func setup() {
        setupUI()
        setupObservers()
        setupTapGesture()
    }
    
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func setupUI() {
        amountTextField.delegate = self
        amountTextField.attributedPlaceholder = NSAttributedString(string: "Amount", attributes: [.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
    }
    
    private func setupTapGesture() {
        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.addTarget(self, action: #selector(didTapView))
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    @objc private func didTapView() {
      self.view.endEditing(true)
    }
    
    // MARK: - Actions
    @IBAction private func startButtonTapped(_ sender: UIButton) {
        
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            addInsetAnimated(keyboardSize.height)
        }
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        addInsetAnimated(0)
    }
    
    private func addInsetAnimated(_ inset: CGFloat) {
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: inset, right: 0)
    }
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        dollarLabel.isHidden = true
        textField.resignFirstResponder()
        return true
    }   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let updatedText = (text as NSString).replacingCharacters(in: range, with: string)
        dollarLabel.isHidden = updatedText.isEmpty
        guard let amount = Double(updatedText) else { return true }
        return amount <= 1000.0
    }
}
