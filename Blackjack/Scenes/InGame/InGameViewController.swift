//
//  InGameViewController.swift
//  Blackjack
//
//  Created by Ernesto Jose Contreras Lopez on 21/7/22.
//

import Foundation
import UIKit

class InGameViewController: BaseViewController {
    // MARK: - Constants
    
    // MARK: - Properties
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setup
    private func setup() {
        self.setupUI()
    }
    
    private func setupUI() {
        
    }
    
    // MARK: - Helpers
    
    // MARK: - Actions
}
